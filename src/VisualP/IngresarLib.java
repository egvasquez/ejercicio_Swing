package VisualP;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;

public class IngresarLib extends JDialog {
    private JPanel contentPane;
    private JTextField Autor;
    private JTextField Edicion;
    private JTextField Titulo;
    private JTextField Editorial;
    private JTable table1;
    private JButton ingresarButton;
    private JButton eliminarButton;
    private JButton eliminarButton1;
    private JButton regresarButton;
    private JButton buttonOK;
    private JButton buttonCancel;
    private DefaultTableModel model;

    public IngresarLib() {
        setContentPane(contentPane);
        setModal(true);

        model=new DefaultTableModel();
        model.addColumn("Autor");
        model.addColumn("Titulo");
        model.addColumn("Edicion");
        model.addColumn("Editorial");
        table1.setModel(model);



        //setResizable(false);
        setTitle("Ingresar Libros");






        //Boton de ingresar
        ingresarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            String Autor1 = Autor.getText();
            String Titulo1 = Titulo.getText();
            String Edicion1 = Edicion.getText();
            String Editorial1 = Editorial.getText();

            Autor.setText("");
            Titulo.setText("");
            Edicion.setText("");
            Editorial.setText("");

            model.addRow(new Object[]{Autor1, Titulo1, Edicion1, Editorial1});

            }
        });


        eliminarButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            System.exit(0);

            }
        });
        regresarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Menu M1 = new Menu();
                M1.setMinimumSize(new Dimension(600,300));
                M1.setLocationRelativeTo(null);
                M1.setVisible(true);
                dispose();
            }
        });
    }


    public static void main(String[] args) {
        IngresarLib dialog = new IngresarLib();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
