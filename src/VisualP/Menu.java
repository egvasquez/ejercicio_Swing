package VisualP;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JFrame;

public class Menu extends JDialog {
    private JPanel contentPane;
    private JButton ingresarLibrosButton;
    private JButton registrarAlumnosYMostrarButton;
    private JButton buttonOK;
    private JButton buttonCancel;

    public Menu() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        ///Menu obj2 =new Menu();
        //obj2.setMinimumSize(new Dimension(900,600));



        setLocationRelativeTo(null);
        setResizable(true);
        setTitle("Menu");
        setMinimumSize(new Dimension(300,150));

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ingresarLibrosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngresarLib obj =new IngresarLib();
                obj.setMinimumSize(new Dimension(900,600));
                obj.setLocationRelativeTo(null);
                obj.setVisible(true);


            }
        });
        registrarAlumnosYMostrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngresarLib obj =new IngresarLib();
                obj.setMinimumSize(new Dimension(900,600));
                obj.setLocationRelativeTo(null);
                obj.setVisible(true);
                dispose();
            }
        });
        registrarAlumnosYMostrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngresarAlum obj1 = new IngresarAlum();
                obj1.setMinimumSize(new Dimension(900,600));
                obj1.setLocationRelativeTo(null);
                obj1.setVisible(true);
                dispose();
            }
        });
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Menu dialog = new Menu();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
