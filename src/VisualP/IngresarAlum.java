package VisualP;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.table.DefaultTableModel;

public class IngresarAlum extends JDialog {
    private JPanel contentPane;
    private JTable table1;
    private JButton ingresarButton;
    private JButton salirButton;
    private JButton eliminarButton;
    private JTextField Nombre;
    private JTextField Apellido;
    private JTextField Carnet;
    private JTextField Lugar;
    private JComboBox sexo;
    private JTextField FechaNac;
    private JButton regresarButton;
    private JButton buttonOK;
    private JButton buttonCancel;
    private DefaultTableModel model;

    public IngresarAlum() {
        setContentPane(contentPane);
        setModal(true);

        model = new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Carnet");
        model.addColumn("Lugar");
        model.addColumn("Sexo");
        model.addColumn("Fecha Nacimiento");
        table1.setModel(model);

        setTitle("Ingresar Alumnos");
        setMinimumSize(new Dimension(900,600));


        getRootPane().setDefaultButton(buttonOK);


        // call onCancel() on ESCAPE
        ingresarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String Nom = Nombre.getText();
                String Apell = Apellido.getText();
                String Carne = Carnet.getText();
                String Lug = Lugar.getText();
                String Sex = (String)sexo.getSelectedItem();
                String dat  = FechaNac.getText();

                Nombre.setText("");
                Apellido.setText("");
                Carnet.setText("");
                Lugar.setText("");
                sexo.setSelectedItem("");
                FechaNac.setText("");

                model.addRow(new Object[]{Nom, Apell, Carne, Lug, Sex, dat});
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });
        salirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        regresarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Menu M = new Menu();
                M.setMinimumSize(new Dimension(300,150));
                M.setLocationRelativeTo(null);
                M.setVisible(true);
                dispose();
            }
        });
    }


    public static void main(String[] args) {
        IngresarAlum dialog = new IngresarAlum();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
        dialog.setMinimumSize(new Dimension(900,600));
    }
}
