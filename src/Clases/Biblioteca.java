package Clases;

/**
 * Created by egust on 8/07/2017.
 */
public class Biblioteca {
    private Libro[] litlibros;
    private Alumno[] listaAlumnos;
    private int cantidadlibros;

    public Biblioteca(int cantidadlibros) {
        litlibros = new Libro[cantidadlibros];
        listaAlumnos = new Alumno[cantidadlibros];
        cantidadlibros = 0;
    }


    public Libro[] getLitlibros() {
        return litlibros;
    }

    public void setLitlibros(Libro[] litlibros) {
        this.litlibros = litlibros;
    }

    public int getCantidadlibros() {
        return cantidadlibros;
    }

    public void setCantidadlibros(int cantidadlibros) {
        this.cantidadlibros = cantidadlibros;


    }

    public Alumno[] getListaAlumnos() {
        return listaAlumnos;
    }

    public void setListaAlumnos(Alumno[] listaAlumnos) {
        this.listaAlumnos = listaAlumnos;
    }


    //un metodo para agregar los libros;
    public void Agregarlibro(Libro l) throws Exception {
        if (cantidadlibros < litlibros.length) {
            for (int i = cantidadlibros; i > 0; i--) {
                litlibros[i] = litlibros[i - 1];
            }
            litlibros[0] = l;
        } else
            litlibros[cantidadlibros] = l;
        cantidadlibros++;

    }

    public void AgregarAlumno(Alumno a) throws Exception {
        if (cantidadlibros < listaAlumnos.length) {
            for (int i = cantidadlibros; i > 0; i--) {
                listaAlumnos[i] = listaAlumnos[i - 1];
            }
            listaAlumnos[0] = a;
        } else
            listaAlumnos[cantidadlibros] = a;
        cantidadlibros++;
    }


}